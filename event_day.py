#!python3
from zabbix.api import ZabbixAPI
import time
from openpyxl import Workbook
from exchangelib.protocol import BaseProtocol, NoVerifyHTTPAdapter
from exchangelib import Credentials, Account, Configuration, DELEGATE, Message, HTMLBody, Mailbox, FileAttachment
BaseProtocol.HTTP_ADAPTER_CLS = NoVerifyHTTPAdapter
import urllib3
urllib3.disable_warnings()
URL='http://127.0.0.1/zabbix'
USER='user'
PASS='pass'
zapi = ZabbixAPI(url=URL, user=USER, password=PASS)

filename = 'event_day.xlsx'
recipientsList=[ Mailbox(email_address="email1.ru") ]
copyList=[ "email2@ru" ]

def time_from_till(unixtime):
    t_till=unixtime
    t_from=unixtime-432000
    r={'from':t_from,'till':t_till}
    return r

event_get=zapi.event.get(
    selectHosts=['hostid','name'],
    groupids=['18'],
    source='0',
    object='0',
    #value='1',
    sortfield=['eventid'],
    sortorder='DESC',
    preservekeys='1',
    search={'name':'High bandwidth usage > 90'},
    time_from=time_from_till(int(time.time()))['from'],
    time_till=time_from_till(int(time.time()))['till'],
    evaltype='0'
)
zapi.user.logout()

# объект
wb = Workbook()

# активный лист
ws = wb.active

#название страницы
ws.title = 'sheet'

ws['A1'].value="Имя хоста"
ws['B1'].value="Проблема"
ws['C1'].value="Время проблемы"
ws['D1'].value="Время восстановления"
i=2

list_keys=list(event_get.keys())
list_keys.sort()
list_objectid=[]
problem='1'
recovery='0'
res_list=[]
p_time=0
e_id=0
for event in list_keys:
    if event_get[event]['objectid'] not in list_objectid:
        list_objectid.append(event_get[event]['objectid'])
        event_get[event]['problem']=0
        event_get[event]['recovery']=0
        p_time=0
        count=0
        for e in list_keys:
            if event_get[event]['objectid'] == event_get[e]['objectid']:
                count+=1
                if event_get[e]['value'] == problem:
                    p_time=event_get[e]['clock']
                    event_get[e]['problem']=p_time
                    e_id=e
                    event_get[e]['e_id']=e_id
                elif event_get[e]['value'] == recovery:
                    event_get[e]['problem']=p_time
                    event_get[e]['e_id']=e_id
                    res_list.append(event_get[e])
        if count == 1:
            res_list.append(event_get[event])

for res in res_list:
    l_url='http://127.0.0.1/zabbix/tr_events.php?triggerid=%s&eventid=%s' % (res['objectid'],res['eventid'])
    ws['A%s' % i].value=res['hosts'][0]['name']
    ws['B%s' % i].value=res['name']
    ws['B%s' % i].hyperlink=l_url
    ws['C%s' % i].value=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(res['problem'])))
    ws['D%s' % i].value=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(res['clock'])))
    i+=1

wb.save(filename)

monReportCredentials = Credentials('emailuser', 'emailpass#')
config = Configuration(server='127.0.0.1', credentials=monReportCredentials)

monReportAccount = Account (
    primary_smtp_address='email@ru', #от этого адреса будет рассылка
    credentials=monReportCredentials, 
    autodiscover=False,
    config = config,
    access_type=DELEGATE
)

message = Message(
        account=monReportAccount,
        subject='Тема письма',        
        body = HTMLBody('во вложении'),
        to_recipients = recipientsList,
        cc_recipients = copyList,  
        )

with open(filename, 'rb') as f:
    my_file = FileAttachment(name=filename, content=f.read())
message.attach(my_file)

message.send()
